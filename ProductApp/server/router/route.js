var express = require('express');
var router = express.Router();
var product = require('../model/productmodel.js');

router.post('/addproduct', (req,res,next)=>{
    let newproduct = {
        product_id : req.body.pid,
        product_name: req.body.name,
        product_price : req.body.price
    }
    product.insertMany(newproduct,(err,response)=>{
        if(err){
            res.json({"msg":"Data not inserted", "error":err});
        }
        else{
            res.json({"msg":"Data inserted", "response":response});
        }
    });});
    router.get('/getproduct',(req,res,next)=>{
        product.find((err,datas)=>{
        if(err){
            res.json({"error":err});
        }
        else{
            res.send(datas);
        }
    })
    })
    router.delete('/delproduct/:id',(req,res,next)=>{
       console.log (req.params.id);
       console.log ( req.url);
        product.remove({_id:req.params.id},(err,response)=>{
        if(err){
            res.json({"error":"err"});
        }
        else{
            res.json({"DAta":"deleted"});
        }
    })
    })
    router.put('/editproduct/:id', (req,res,next)=>{
    let editproduct = {
        product_id : req.body.pid,
        product_name: req.body.name,
        product_price : req.body.price
    }
    product.update({_id:req.params.id},{$set:editproduct},(err,response)=>{
        if(err){
            res.json({"msg":"Data not inserted", "error":err});
        }
        else{
            res.json({"msg":"Data inserted", "response":response});
        }
    });});
    router.get('/getproductbyid/:id',(req,res,next)=>{
        product.find({_id:req.params.id},(err,datas)=>{
        if(err){
            res.json({"error":err});
        }
        else{
            res.json({"DAta":datas});
        }
    })
    })
module.exports = router;