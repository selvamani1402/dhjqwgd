var express = require('express');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var cors = require('cors');

 var app = express();
 const PORT = 4000;
var router = require('./router/route.js')
 var dbpath = "mongodb://localhost:27017/products";
 mongoose.connect(dbpath,(err,db)=>{
     if(err){
         console.log("DB NOT CONNECTED" +Error)
     }
     else{
         console.log("DB CONNECTED");
         
     }
 });

 // link middleware

app.use(bodyparser.json());
app.use(cors());
app.use('/app', router);

//  app.get('/register' , (req,res)=>{
//      console.log(req.url);
//      res.send("Register Success");
//  }) 
//  app.get('/home' , (req,res)=>{
//     console.log(req.url);
//     res.send("Welcome to my APP");
// }) 
 app.listen(PORT, ()=>{
     console.log("SERVER STARTED AT PORT :" +PORT);
 })