var mongoose = require('mongoose');

var productSchema = mongoose.Schema({
    product_id:{
            type:String,
            require:true
    },
    product_name:{
        type:String,
        require:true
},
    product_price:{
         type:Number,
         require:true
}
});
var product =module.exports = mongoose.model("products" , productSchema);