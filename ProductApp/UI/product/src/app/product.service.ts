import { Injectable } from '@angular/core';
import {map} from 'rxjs/operators';
import {HttpModule, Http} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpObj: Http) {}
    createProduct(prod){
      return this.httpObj.post('http://localhost:4000/app/addproduct',prod).pipe(map(res=> res.json()));
    
   }
   showProduct() {
    return this.httpObj.get('http://localhost:4000/app/getproduct').pipe(map(res => res.json()));
  }
  editProduct(id,prod){
    return this.httpObj.put('http://localhost:4000/app/editproduct/' +id,prod).pipe(map(res => res.json()));
}
  deleteProduct(prodID){
  return this.httpObj.delete('http://localhost:4000/app/delproduct/' +prodID).pipe(map(res => res.json()));
  }
  viewProduct(id){
    return this.httpObj.get('http://localhost:4000/app/getproductbyid/' + id).pipe(map(res => res.json()));
  
}
}