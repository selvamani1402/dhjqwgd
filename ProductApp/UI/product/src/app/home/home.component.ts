import { Component, OnInit } from '@angular/core';
import {Product} from '../product';
import { ProductService } from '../product.service';
import {map} from 'rxjs/operators';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
products: Product[];
 proId:string;
 proName:string;
 proPrice:number;
 id:string;
 

  constructor(private serObj: ProductService) { }

  ngOnInit() {
    this.display();
   
  }

  // myfun(){
  //   console.log("hello");
  //   alert("Hai");
  // }
  
  display(){
    this.serObj.showProduct().subscribe(
    (res)=>{
      this.products = res;
      
    },
    (err)=>{
      console.log("Error Occured");
    }
    );
  }
  edit(product){
    this.proId=product.product_id;
    this.proName=product.product_name;
    this.proPrice=product.product_price;
    this.id=product._id;
  }
save(editproduct){
console.log(this.id);
console.log(editproduct);
this.serObj.editProduct(this.id,editproduct).subscribe(
  (res)=>{
this.display();
  },
  (err)=>{

  }
)
}



    addProduct(newproduct){
console.log(newproduct);
this.serObj.createProduct(newproduct).subscribe(
  (res)=>{
    this.display();
  },
  (err)=>{
    console.log(err);
  }
);
    }
 delProduct(objid){

    this.serObj.deleteProduct(objid).subscribe(
      (res)=>{
        console.log(res);
        alert("data deleted")
        this.display();

    
      },
      (err)=>{
          console.log(err);    
      }
    )
  
    }    
}